package Ctrl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Classe principale contenant le main
 * @author Benjamin
 *
 */
public class Main {
	
	/**
	 * Logger pour les logs 
	 */
	final static Logger logger =LogManager.getLogger(Main.class);
	

	public static void main(String[] args) {
		try {
			logger.info("Demarrage du programme");
			Lancement l=new Lancement();
			if(args.length>0&&args[0].equals("modedev"))
				l.menuApp(true);
			else 
				l.menuApp(false);
		}catch(Exception e) {
			System.out.println(e);
			logger.error("Quelque chose ne va pas : " + e);
			logger.info("Fin du programme");
		}
		
		
	}
	
	
}
