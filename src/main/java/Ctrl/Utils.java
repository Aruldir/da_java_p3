package Ctrl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Classe contenant des M�thodes/Fonctions Utilitaires.
 * @author Benjamin
 */
public class Utils {
	/**
	 * Le scanneur pour entrer les choix
	 */
	private static Scanner sc=new Scanner(System.in);
	static Properties prop =new Properties();
	final static Logger logger =LogManager.getLogger(Utils.class);



	/**
	 * Fonction servant � verifier si la valeur entr�e et entre inf et sup
	 * @param inf
	 * 		valeur inf�rieur
	 * @param sup
	 * 		valeur sup�rieur
	 * @return la valeur v�rifi�
	 */
	public static int verifentree(int inf,int sup) {
		int entree;
		String infs=inf+"";
		String sups=sup+"";
		String e;
		e=sc.next();
		while( e.compareTo(infs)<0||e.compareTo(sups)>0) {
			System.out.println("Erreur!!! \n"
					+ "Entrez un nombre entre "+inf+" et "+sup);
			e=sc.next();
		}
		entree=Integer.parseInt(e);
		return entree;
	}


	/**
	 * Fonction servant � lire , dans le fichier "config.properties", les valeurs des cl�s entier�res.<br>
	 * Elle v�rifie aussi les valeurs des cl�s.
	 * @param nomdelacle
	 * 		Nom de la cl� 
	 * @return la valeur dans le fichier ou null si la valeur est incorrecte.
	 * 
	 */
	public static int recuperationvaleurInt(String nomdelacle) {
		InputStream in;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();


		try {
			in = classloader.getResourceAsStream("config.properties");
			prop.load(in);
			if(nomdelacle.equals("Recherche.nbatrouver")||nomdelacle.equals("Mastermind.nbatrouver")||nomdelacle.equals("Mastermind.nbcouleur")) {
				if(Integer.parseInt(prop.getProperty(nomdelacle))<4||Integer.parseInt(prop.getProperty(nomdelacle))>10) {
					logger.error("Valeur incorrect !");
					logger.debug("Verifiez que les valeurs pour nbcouleur et nbatrouver soit entre 4 et 10");
				}
			}

			return Integer.parseInt(prop.getProperty(nomdelacle));


		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("Le fichier n'existe pas : "+ e);
			logger.info("Fin du programme");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Il n'y a pas de valeur : "+ e);
			logger.debug("Verifiez le fichier config.properties");
			logger.info("Fin du programme");
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error("La valeur n'est pas un entier : "+ e);
			logger.debug("Verifiez le fichier config.properties et qu'il n'y est pas d'espace apres la valeur");
			logger.info("Fin du programme");
		}
		return (Integer) null;
	}

	/**
	 * Fonction servant � lire , dans le fichier "config.properties", les valeurs des cl�s bol�enne.<br>
	 * Elle v�rifie aussi les valeurs des cl�s.
	 * @param nomdelacle
	 * 		Nom de la cl� 
	 * @return la valeur dans le fichier ou null si la valeur est incorrecte.
	 * 
	 */
	public static boolean recuperationvaleurBoolean(String nomdelacle) {
		InputStream in;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();


		try {
			in = classloader.getResourceAsStream("config.properties");
			prop.load(in);
			if(nomdelacle.equals("Recherche.modedev")||nomdelacle.equals("Mastermind.modedev")) {
				if(prop.getProperty("Recherche.modedev").equals("true")||prop.getProperty("Recherche.modedev").equals("false")||prop.getProperty("Mastermind.modedev").equals("true")||prop.getProperty("Mastermind.modedev").equals("false")) {
					logger.error("Valeur incorrect !");
					logger.debug("Verifiez que les valeurs pour modedev soit true ou false");
				}
			}

			return Boolean.parseBoolean(prop.getProperty(nomdelacle));


		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("Le fichier n'existe pas : "+ e);
			logger.info("Fin du programme");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Il n'y a pas de valeur : "+ e);
			logger.debug("Verifiez le fichier config.properties");
			logger.info("Fin du programme");
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error("La valeur n'est pas un boolean : "+ e);
			logger.debug("Verifiez le fichier config.properties et qu'il n'y est pas d'espace apres la valeur");
			logger.info("Fin du programme");
		}
		return (Boolean) null;
	}


}
