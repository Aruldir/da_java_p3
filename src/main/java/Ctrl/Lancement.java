package Ctrl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Jeu.Mastermind;
import Jeu.Recherche;

/**
 * Classe servant de Lancement du menu.
 * 
 * @author Benjamin
 *
 */
public class Lancement {

	final static Logger logger =LogManager.getLogger(Lancement.class);
	int choix;
	
	/**
	 * Constructeur par d�faut.
	 */
	public Lancement() {
		this.choix=-1; //Aucun choix
	}
	
	/**
	 * M�thode pour l'affichage du menu de l'application.
	 * @param modedev
	 * 		Mode dev : Vrai pour l'activer, Faux pour regarder dans le dossier de config
	 */
	public void menuApp(boolean modedev) {
		System.out.println("-----------------------------\n"
				+ "Bienvenue Veuillez choisir un jeu :\n"
				+ "1. Recherche +/- \n"
				+ "2. Mastermind \n"
				+ "3. Quitter");
		choix=Utils.verifentree(1, 3);
		//Recherche +/-
		if(choix==1) {
			Recherche game=new Recherche();
			game.menu(modedev);
		}
		if(choix==2) {
			Mastermind game=new Mastermind();
			game.menu(modedev);
		}
		if(choix==3) {
			logger.info("Fin du programme");
			System.exit(0);
		}
	}
}
