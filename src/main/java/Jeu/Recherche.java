package Jeu;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Ctrl.*;

/**
 * Classe repr�sentant le jeu Recherche +/-
 * 
 * @author Benjamin
 *
 */
public class Recherche {

	/**
	 * Le boolean servant � savoir si l'utilisateur � gagner.
	 */
	private boolean winu;

	/**
	 * Le boolean servant � savoir si l'ordinateur � gagner.
	 */
	private boolean wino;

	/**
	 * Le tableau contenant la combinaison que doit trouver l'utilisateur.
	 */
	private int[] combinaisonu;

	/**
	 * Le tableau contenant la combinaison que doit trouver l'ordinateur.
	 */
	private int[] combinaisono;

	/**
	 * Le tableau contenant la proposition de l'utilisateur.
	 */
	private int[] propositionu;

	/**
	 * Le tableau contenant la proposition de l'ordinateur.
	 */
	private int[] propositiono;

	/**
	 * Le tableau contenant les r�ponses de l'utilisateur sous forme de "+,-,="
	 */
	private char[] reponseu;

	/**
	 * Le tableau contenant les r�ponses de l'ordinateur sous forme de "+,-,="
	 */
	private char[] reponseo;

	/**
	 * Le scanneur pour entrer les choix.
	 */
	private Scanner sc=new Scanner(System.in);

	/**
	 * Le nombre de tour.
	 */
	int tour;

	/**
	 * La borne inf�rieur lors de la proposition de l'ordinateur.
	 * 
	 * @see Recherche#proposi(int, String)
	 */
	int[] borneinferieur;

	/**
	 * La borne sup�rieur lors de la proposition de l'ordinateur.
	 * 
	 * @see Recherche#proposi(int, String)
	 */
	int[] bornesuperieur;
	
	/**
	 * Le logger servant pour les logs.
	 */
	final static Logger logger =LogManager.getLogger(Recherche.class);
	
	/**
	 * Constructeur par d�faut.
	 */
	public Recherche() {
		logger.info("D�but du jeu recherche");
	}

	/**
	 * M�thode servant � afficher le menu du Jeu Recherche.
	 * @param modedev
	 * 		Mode dev : Vrai pour l'activer, Faux pour regarder dans le dossier de config
	 */
	public void menu(boolean modedev) {
		int choix=0;
		
		System.out.println("----------------\n"
				+ "Bienvenue dans le Jeu Recherche +- \n"
				+ "Choisissez un mode :\n"
				+ "1. Challenger \n"
				+ "2. Defenseur \n"
				+ "3. Duel ");
		choix=Utils.verifentree(1, 3);
		//challenger-Recherche
		if(choix==1) {
			logger.info("D�but du mode challenger");
			if(modedev==true) 
				this.start("challenger", Utils.recuperationvaleurInt("Recherche.nbatrouver"), true, Utils.recuperationvaleurInt("Recherche.nbessai"));
				
			else
				this.start("challenger", Utils.recuperationvaleurInt("Recherche.nbatrouver"), Utils.recuperationvaleurBoolean("Recherche.modedev"), Utils.recuperationvaleurInt("Recherche.nbessai"));
			
		}
		//defenseur-Recherche
		if(choix==2) {
			logger.info("D�but du mode defenseur");
			this.start("defenseur", Utils.recuperationvaleurInt("Recherche.nbatrouver"), false, Utils.recuperationvaleurInt("Recherche.nbessai"));
		}
		//duel-Recherche
		if(choix==3) {
			logger.info("D�but du mode duel");
			if(modedev==true) 
				this.start("duel", Utils.recuperationvaleurInt("Recherche.nbatrouver"), true, Utils.recuperationvaleurInt("Recherche.nbessai"));
				
			else
				this.start("duel", Utils.recuperationvaleurInt("Recherche.nbatrouver"), Utils.recuperationvaleurBoolean("Recherche.modedev"), Utils.recuperationvaleurInt("Recherche.nbessai"));
		}
	}



	
	/**
	 * M�thode servant � d�marrer une partie de Recherche
	 * @param modedej
	 * 		Mode de jeu choisi
	 * @param nbatrouver
	 * 		Nombre de chiffres � trouver pour gagner
	 * @param modedev
	 * 		Mode dev : Vrai pour l'activer, Faux pour ne pas l'activer
	 * @param nbessai
	 * 		Nombre d'essai avant de perdre 
	 */
	private void start(String modedej,int nbatrouver, boolean modedev, int nbessai ) {
		this.propositionu=new int[nbatrouver];
		this.propositiono=new int[nbatrouver];
		this.bornesuperieur=new int[nbatrouver];
		this.borneinferieur=new int[nbatrouver];
		this.wino=false;
		this.winu=false;
		this.tour=0;
		int choix=0;
		
		switch (modedej) {

		case "challenger":
			generation(nbatrouver,modedej);
			if(modedev==true) {
				System.out.println("(Combinaison secr�te : "+affichersoluce(nbatrouver)+")");
			}
			while (winu==false&&tour<nbessai) {
				System.out.println("\n "
						+ "Tour : "+ (this.tour+1) +"\n");
				proposi(nbatrouver,modedej);
				verification(nbatrouver,modedej);
				gagner(nbatrouver,modedej);
				this.tour++;
			}
			if(winu==true)
				System.out.println("Vous avez gagn� en "+this.tour +" tours\n");
			else
				System.out.println("Vous avez perdu \n");
			
			System.out.println("----------------\n"
					+ "Voulez-vous rejouer ?\n"
					+ "1. Oui \n"
					+ "2. Non");
			choix=Ctrl.Utils.verifentree(1, 2);
			if(choix==1) {
				logger.info("Restart du mode challengeur");
				start(modedej, nbatrouver, modedev,nbessai);
			}
			if(choix==2) {
				logger.info("Fin du mode challenger");
				Lancement l=new Lancement();
				l.menuApp(modedev);
			}
			break;

		case "defenseur":
			generation(nbatrouver, modedej);
			while (wino==false&&tour<nbessai) {
				System.out.println("\n "
						+ "Tour : "+ (this.tour+1) +"\n");
				proposi(nbatrouver,modedej);
				verification(nbatrouver,modedej);
				gagner(nbatrouver,modedej);
				this.tour++;
				if(wino==false) {
					System.out.println("Taper sur une touche pour continuer");
					sc.next();
				}



			}
			if(wino==true)
				System.out.println("L'ordinateur a gagn� en "+this.tour +" tours\n");
			else
				System.out.println("Vous avez perdu \n");
			System.out.println("----------------\n"
					+ "Voulez-vous rejouer ?\n"
					+ "1. Oui \n"
					+ "2. Non");
			choix=Ctrl.Utils.verifentree(1, 2);
			if(choix==1) {
				logger.info("Restart du mode d�fensseur");
				start(modedej, nbatrouver, modedev,nbessai);
			}
			if(choix==2) {
				logger.info("Fin du mode defenseur");
				Lancement l=new Lancement();
				l.menuApp(modedev);
			}
			break;
		case "duel" :
			generation(nbatrouver,"defenseur");
			generation(nbatrouver,"challenger");
			if(modedev==true) {
				System.out.println("(Combinaison secr�te : "+affichersoluce(nbatrouver)+")");
			}
			while (wino==false&&winu==false&&tour<nbessai) {
				System.out.println("\n"
						+ "Tour : "+ (this.tour+1) +"\n");
				proposi(nbatrouver, "challenger");
				verification(nbatrouver, "challenger");
				gagner(nbatrouver,"challenger");

				if(winu==false) {
					proposi(nbatrouver, "defenseur");
					verification(nbatrouver, "defenseur");
					gagner(nbatrouver,"defenseur");
				}

				this.tour++;
			}
			if(winu==true) 
				System.out.println("Vous avez gagn� en "+this.tour +" tours\n");
			else if(wino==true) 
				System.out.println("L'ordinateur a trouv� en "+this.tour +" tours\n");
			else 
				System.out.println("Tout le monde a perdu ! \n");
			System.out.println("----------------\n"
					+ "Voulez-vous rejouer ?\n"
					+ "1. Oui \n"
					+ "2. Non");
			choix=Ctrl.Utils.verifentree(1, 2);
			if(choix==1) {
				logger.info("Restart du mode duel");
				start(modedej, nbatrouver, modedev, nbessai);
			}
			if(choix==2) {
				logger.info("Fin du mode duel");
				Lancement l=new Lancement();
				l.menuApp(modedev);
			}
			break;
		default:

			break;
		}
	}



	/**
	 * M�thode servant � g�n�rer le nombre � trouver <br>
	 * Si on est en mode Challenger, l'ordinateur g�nere la combinaison. <br>
	 * Si on est en mode defenseur, la combinaison sera demand� � l'utilisateur.
	 * 
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param modedej
	 * 			Mode de Jeu choisi
	 */
	private void generation(int nbatrouver, String modedej) {
		switch (modedej) {
		case "challenger":
			this.combinaisonu=new int[nbatrouver];
			int[] tabgenere =new int[nbatrouver];

			for(int i=0;i<nbatrouver;i++){
				tabgenere[i]=(int)(Math.random() * (10-0)) + 0;

			}

			this.combinaisonu=tabgenere;
			break;
		case "defenseur":
			this.combinaisono=new int[nbatrouver];
			System.out.println("\n"
					+ "Bienvenue !!!\n"
					+ "Taper la combinaison � trouver pour l'ordinateur :");
			String[] pro =sc.next().split("");
			while(pro.length!=nbatrouver) {
				System.out.println("Erreur !! Proposition : ");
				pro =sc.next().split(""); 
			}
			for (int i=0;i<nbatrouver;i++) {
				this.combinaisono[i]=Integer.parseInt(pro[i]);
			}
			break;

		default:

			break;

		}
	}

	/**
	 * M�thode servant � faire une proposition. <br>
	 * Si on est en mode Challenger, la proposition sera entr�e par l'utilisateur.<br>
	 * Si on est en mode Defenseur, la proposition sera g�n�r� par l'ordinateur.
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param modedej
	 * 			Mode de Jeu a laquelle on joue
	 */
	private void proposi(int nbatrouver,String modedej) {

		switch (modedej) {
		case "challenger":
			System.out.println("Proposition : ");
			String[] pro =sc.next().split("");
			while(pro.length!=nbatrouver) {
				System.out.println("Erreur !! Proposition : ");
				pro =sc.next().split(""); 
			}
			for (int i=0;i<nbatrouver;i++) {
				this.propositionu[i]=Integer.parseInt(pro[i]);
			}
			break;

		case "defenseur":
			String proordi="";

			switch (this.tour) {
			case 0:
				proordi="";
				for(int i=0;i<nbatrouver;i++) {
					this.propositiono[i]=0;
					borneinferieur[i]=0;
					proordi=proordi+this.propositiono[i];
				}
				System.out.println("L'ordinateur a propos� : "+ proordi);
				break;

			case 1: 
				proordi="";
				for(int i=0;i<nbatrouver;i++) {
					if(this.reponseo[i]=='=') this.propositiono[i]=this.propositiono[i];
					if(this.reponseo[i]=='+') {

						this.propositiono[i]=9;
						bornesuperieur[i]=9;
					}

					proordi=proordi+this.propositiono[i];
				}
				System.out.println("L'ordinateur a propos� : "+ proordi);
				break;

			default :
				proordi="";

				for(int i=0;i<nbatrouver;i++) {
					if(this.reponseo[i]=='=') this.propositiono[i]=this.propositiono[i];

					if(this.reponseo[i]=='+') {
						this.borneinferieur[i]=this.propositiono[i];
						this.propositiono[i]=(borneinferieur[i]+bornesuperieur[i])/2;

					}

					if(this.reponseo[i]=='-') {
						this.bornesuperieur[i]=this.propositiono[i];
						this.propositiono[i]=(borneinferieur[i]+bornesuperieur[i])/2;

					}

					proordi=proordi+this.propositiono[i];


				}
				System.out.println("L'ordinateur a propos� : "+ proordi);
				break;
			}

			break;
		}

	}

	/**
	 * M�thode servant � verifier les propositions de l'ordinateur et de l'utilisateur selon le mode choisi.
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param modedej
	 * 			Mode de Jeu a laquelle on joue
	 */
	private void verification(int nbatrouver,String modedej) {
		String rep="";


		if(modedej.compareTo("challenger")==0) {
			reponseu=new char[nbatrouver];
			for(int i=0;i<nbatrouver;i++) {
				if(propositionu[i]==combinaisonu[i]) reponseu[i]='=';


				if(propositionu[i]<combinaisonu[i]) reponseu[i]='+';

				if(propositionu[i]>combinaisonu[i]) reponseu[i]='-';

				rep=rep+reponseu[i];
			}
			System.out.println("Reponse : "+rep);
		}

		if(modedej.compareTo("defenseur")==0) {
			reponseo=new char[nbatrouver];
			for(int i=0;i<nbatrouver;i++) {
				if(propositiono[i]==combinaisono[i]) reponseo[i]='=';


				if(propositiono[i]<combinaisono[i]) reponseo[i]='+';

				if(propositiono[i]>combinaisono[i]) reponseo[i]='-';

				rep=rep+reponseo[i];
			}
			System.out.println("Reponse : "+rep);
		}


	}
	/**
	 * M�thode servant � savoir si l'utilisateur ou l'ordinateur � gagner.
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param modedej
	 * 			Mode de Jeu a laquelle on joue
	 */
	private void gagner(int nbatrouver,String modedej) {
		if(modedej.compareToIgnoreCase("challenger")==0) {
			for(int i=0;i<nbatrouver;i++) {
				if(reponseu[i]!='=') {
					this.winu=false;
					break;
				}
				else 
				{
					this.winu=true;

				}
			} 
		}


		if(modedej.compareTo("defenseur")==0) {
			for(int i=0;i<nbatrouver;i++) {
				if(reponseo[i]!='=') {
					this.wino=false;
					break;
				}
				else this.wino=true;
			} 
		}
	}

	/**
	 * Fonction servant � mettre sous forme d'une chaine de caract�re la solution
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @return soluce
	 * 			Chaine de caract�re de la soluce
	 */
	private String affichersoluce(int nbatrouver) {
		String soluce="";
		for(int i=0;i<nbatrouver;i++) {

			soluce=soluce+this.combinaisonu[i];
		}
		return soluce;
	}





}
