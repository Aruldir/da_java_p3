package Jeu;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Ctrl.Lancement;
import Ctrl.Utils;

/**
 * Classe repr�sentant le jeu Mastermind.
 * 
 * @author Benjamin
 *
 */
public class Mastermind {

	/**
	 * Le nombre de bien plac�s de l'utilisateur.
	 */
	private int bienplaceu;

	/**
	 * Le nombre de pr�sents de l'utilisateur.
	 */
	private int presentu;

	/**
	 * Le nombre de pr�sents de l'ordinateur.
	 */
	private int presento;

	/**
	 * Le nombre de bien plac�s de l'ordinateur.
	 */
	private int bienplaceo;

	/**
	 * Le nombre de tours.
	 */
	private int tour=0;

	/**
	 * Le tableau contenant la combinaison que doit trouver l'utilisateur.
	 */
	private int[] combinaisonu;

	/**
	 * Le tableau contenant la combinaison que doit trouver l'ordinateur.
	 */
	private int[] combinaisono;

	/**
	 * Le tableau contenant la proposition de l'utilisateur.
	 */
	private int[] propositionu;

	/**
	 * Le tableau contenant la proposition de l'utilisateur.
	 */
	private int[] propositiono;

	/**
	 * L'ArrayList contenant toutes les possibilit�s d'une parties.
	 */
	private ArrayList<int[]> toutepos;

	/**
	 * Le scanneur pour entrer les choix.
	 */
	private Scanner sc=new Scanner(System.in);

	final static Logger logger =LogManager.getLogger(Mastermind.class);

	/**
	 * Constructeur par d�faut.
	 */
	public Mastermind() {
		logger.info("D�but du jeu mastermind");
	}

	/**
	 * M�thode servant � d�marrer une partie de Mastermind.
	 * @param modedej
	 * 		Mode de jeu choisi
	 * @param nbatrouver
	 * 		Nombre de chiffres � trouver pour gagner
	 * @param nbcouleur
	 * 		Nombre de couleur dans la partie
	 * @param modedev
	 * 		Mode dev : Vrai pour l'activer, Faux pour regarder dans le dossier de config
	 * @param nbessai
	 * 		Nombre d'essai avant de perdre 
	 */
	private void start(String modedej,int nbatrouver,int nbcouleur,boolean modedev, int nbessai) {
		this.propositionu=new int[nbatrouver];
		this.propositiono=new int[nbatrouver];
		this.toutepos=new ArrayList<int[]>();
		this.tour=0;
		int choix=0;
		boolean modetriche;

		switch(modedej) {
		case "challenger":
			this.bienplaceu=0;
			this.presentu=0;
			generation(modedej,nbatrouver,nbcouleur);
			if(modedev==true) {
				System.out.println("(Combinaison secr�te : "+affichersoluce(nbatrouver)+")");
			}

			while(bienplaceu!=nbatrouver&&tour<nbessai) {
				System.out.println("\n "
						+ "Tour : "+ (this.tour+1) +"\n");
				proposi(nbatrouver,nbcouleur,modedej,false);
				verification(nbatrouver,modedej);
				this.tour++;
			}
			if(bienplaceu==nbatrouver)
				System.out.println("Vous avez gagn� en "+this.tour +" tours\n");
			else if(nbessai==tour)
				System.out.println("Vous avez perdu \n");
			System.out.println("----------------\n"
					+ "Voulez-vous rejouer ?\n"
					+ "1. Oui \n"
					+ "2. Non");
			choix=Ctrl.Utils.verifentree(1, 2);
			if(choix==1) {
				logger.info("Restart du mode challenger");
				start(modedej, nbatrouver, nbcouleur,modedev,nbessai);
			}
			if(choix==2) {
				logger.info("Fin du mode challenger");
				Lancement l=new Lancement();
				l.menuApp(modedev);
			}
			break;

		case "defenseur":
			this.bienplaceo=0;
			this.presento=0;
			modetriche=new Boolean(false);
			System.out.println("Voulez-vous activez le mode triche pour l'ordinateur ? \n"
					+ "1.Oui\n"
					+ "2.Non");
			choix = Utils.verifentree(1, 2);
			if(choix==1) 
				modetriche=true;
			if(choix==2)
				modetriche=false;
			generation(modedej,nbatrouver,nbcouleur);
			genererpos(nbatrouver, nbcouleur);
			while (bienplaceo!=nbatrouver&&tour<nbessai) {
				System.out.println("\n "
						+ "Tour : "+ (this.tour+1) +"\n");
				proposi(nbatrouver,nbcouleur,modedej,modetriche);
				verification(nbatrouver,modedej);
				suppresioncombi(nbatrouver, nbcouleur, modetriche);
				propositiono=new int[nbatrouver];
				this.tour++;
				if(bienplaceo!=nbatrouver) {
					System.out.println("Taper sur une touche pour continuer");
					sc.next();
				}
			}

			if(bienplaceo==nbatrouver)
				System.out.println("L'ordinateur a trouv� en "+this.tour +" tours\n");
			else if(nbessai==tour)
				System.out.println("L'ordinateur a perdu !\n");

			System.out.println("----------------\n"
					+ "Voulez-vous rejouer ?\n"
					+ "1. Oui \n"
					+ "2. Non");
			choix=Ctrl.Utils.verifentree(1, 2);
			if(choix==1) {
				logger.info("Restart du mode defenseur");
				start(modedej, nbatrouver, nbcouleur,modedev,nbessai);
			}
			if(choix==2) {
				logger.info("Fin du mode defenseur");
				Lancement l=new Lancement();
				l.menuApp(modedev);
			}
			break;

		case "duel" :
			this.bienplaceu=0;
			this.presentu=0;
			this.bienplaceo=0;
			this.presento=0;
			modetriche=new Boolean(false);
			System.out.println("Voulez-vous activez le mode triche pour l'ordinateur ? \n"
					+ "1.Oui\n"
					+ "2.Non");
			choix = Utils.verifentree(1, 2);
			if(choix==1) 
				modetriche=true;
			if(choix==2)
				modetriche=false;
			generation("defenseur",nbatrouver,nbcouleur);
			genererpos(nbatrouver, nbcouleur);
			generation("challenger",nbatrouver,nbcouleur);
			
			if(modedev==true) {
				System.out.println("(Combinaison secr�te : "+affichersoluce(nbatrouver)+")");
			}
			while (bienplaceo!=nbatrouver&&bienplaceu!=nbatrouver&&tour<nbessai) {
				System.out.println("\n "
						+ "Tour : "+ (this.tour+1) +"\n");
				//Humain
				proposi(nbatrouver,nbcouleur,"challenger",false);
				verification(nbatrouver,"challenger");
				System.out.println();
				//Ordi
				if(bienplaceu!=nbatrouver) {
					proposi(nbatrouver,nbcouleur,"defenseur",modetriche);
					verification(nbatrouver,"defenseur");
					suppresioncombi(nbatrouver, nbcouleur, modetriche);
					propositiono=new int[nbatrouver];

				}
				this.tour++;
			}
			if(bienplaceu==nbatrouver) {
				System.out.println("Vous avez gagn� en "+this.tour +" tours\n");
			}	
			else if (bienplaceo==nbatrouver) {
				System.out.println("L'ordinateur a trouv� en "+this.tour +" tours\n");
			}else if(nbessai==tour) {
				System.out.println("Vous avez perdu tous les deux perdus !");
			}
			System.out.println("----------------\n"
					+ "Voulez-vous rejouer ?\n"
					+ "1. Oui \n"
					+ "2. Non");
			choix=Ctrl.Utils.verifentree(1, 2);
			if(choix==1) {
				logger.info("Restart du mode duel");
				start(modedej, nbatrouver, nbcouleur,modedev,nbessai);
			}
			if(choix==2) {
				logger.info("Fin du mode duel");
				Lancement l=new Lancement();
				l.menuApp(modedev);
			}
			break;
		}
	}

	/**
	 * M�thode servant � afficher le menu du Jeu Mastermind.
	 * 
	 * @param modedev
	 * 		Mode dev : Vrai pour l'activer, Faux pour regarder dans le dossier de config
	 */
	public void menu(boolean modedev) {
		int choix=0;

		System.out.println("----------------\n"
				+ "Bienvenue dans le jeu Mastermind \n"
				+ "Choisissez un mode :\n"
				+ "1. Challenger \n"
				+ "2. Defenseur \n"
				+ "3. Duel ");
		choix=Utils.verifentree(1, 3);
		//challenger-Mastermind
		if(choix==1) {
			if(modedev==true) 
				start("challenger", Utils.recuperationvaleurInt("Mastermind.nbatrouver"), Utils.recuperationvaleurInt("Mastermind.nbcouleur"),true, Utils.recuperationvaleurInt("Mastermind.nbessai"));

			else
				start("challenger", Utils.recuperationvaleurInt("Mastermind.nbatrouver"), Utils.recuperationvaleurInt("Mastermind.nbcouleur"),Utils.recuperationvaleurBoolean("Mastermind.modedev") , Utils.recuperationvaleurInt("Mastermind.nbessai"));

		}
		//Defenseur-Mastermind
		if(choix==2) {
				start("defenseur", Utils.recuperationvaleurInt("Mastermind.nbatrouver"), Utils.recuperationvaleurInt("Mastermind.nbcouleur"),false, Utils.recuperationvaleurInt("Mastermind.nbessai"));
		}
		if(choix==3) {
			if(modedev==true) 
				start("duel", Utils.recuperationvaleurInt("Mastermind.nbatrouver"), Utils.recuperationvaleurInt("Mastermind.nbcouleur"),true, Utils.recuperationvaleurInt("Mastermind.nbessai"));

			else
				start("duel", Utils.recuperationvaleurInt("Mastermind.nbatrouver"), Utils.recuperationvaleurInt("Mastermind.nbcouleur"),Utils.recuperationvaleurBoolean("Mastermind.modedev") , Utils.recuperationvaleurInt("Mastermind.nbessai"));
		}
	}

	/**
	 * /**
	 * M�thode servant � g�n�rer le nombre � trouver <br>
	 * Si on est en mode Challenger, l'ordinateur g�nere la combinaison. <br>
	 * Si on est en mode defenseur, la combinaison sera demand� � l'utilisateur.
	 * 
	 * @param modedej 
	 * 				Mode de Jeu choisi.
	 * @param nbatrouver
	 * 				Nombre de chiffre � trouver pour gagner.
	 * @param nbcouleur
	 * 				Nombre de couleur dans la partie.
	 */
	private void generation(String modedej, int nbatrouver, int nbcouleur) {

		switch(modedej) {
		case "challenger":
			this.combinaisonu=new int[nbatrouver];
			int[] tabgenere =new int[nbatrouver];

			for(int i=0;i<nbatrouver;i++){
				tabgenere[i]=(int)(Math.random() * ((nbcouleur)-0)) + 0;

			}

			this.combinaisonu=tabgenere;
			break;

		case "defenseur":
			this.combinaisono=new int[nbatrouver];
			System.out.println("\n"
					+ "Bienvenue !!!\n"
					+ "Taper la combinaison � trouver pour l'ordinateur :");
			String[] pro =sc.next().split("");
			while(pro.length!=nbatrouver) {
				System.out.println("Erreur !! Proposition : ");
				pro =sc.next().split("");  
			}
			for(int i=0;i<nbatrouver;i++) {

				if(Integer.parseInt(pro[i])>nbcouleur-1) {
					System.out.println("Erreur sur le nombre "+ pro[i] +" !! Proposition sur ce nombre :");
					pro[i]=sc.next();
					while(Integer.parseInt(pro[i])>nbcouleur-1) {
						System.out.println("Erreur sur le nombre "+ pro[i] +" !! Proposition sur ce nombre :");
						pro[i]=sc.next();
					}
				}
			}
			System.out.print("Proposition sans Erreur : ");
			for (int i=0;i<nbatrouver;i++) {
				this.combinaisono[i]=Integer.parseInt(pro[i]);
				System.out.print(pro[i]);	
			}
			System.out.print("\n");
			break;

		}

	}

	/**
	 * M�thode servant � faire une proposition. <br>
	 * Si on est en mode Challenger, la proposition sera entr�e par l'utilisateur.<br>
	 * Si on est en mode Defenseur, la proposition sera g�n�r� par l'ordinateur.
	 *
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param nbcouleur
	 * 			Nombre de couleur dans la partie
	 * @param modedej
	 * 			Mode de Jeu a laquelle on joue
	 * @param triche
	 * 			Mode triche pour l'ordinateur : Vrai pour l'activer, Faux pour ne pas l'activer
	 */			
	private void proposi(int nbatrouver,int nbcouleur,String modedej,boolean triche) {

		switch (modedej) {
		case "challenger":

			System.out.println("Proposition : ");
			String[] pro =sc.next().split("");
			while(pro.length!=nbatrouver) {
				System.out.println("Erreur !! Proposition : ");
				pro =sc.next().split("");  
			}
			for(int i=0;i<nbatrouver;i++) {

				if(Integer.parseInt(pro[i])>nbcouleur-1) {
					System.out.println("Erreur sur le nombre "+ pro[i] +" !! Proposition sur ce nombre :");
					pro[i]=sc.next();
					while(Integer.parseInt(pro[i])>nbcouleur-1) {
						System.out.println("Erreur sur le nombre "+ pro[i] +" !! Proposition sur ce nombre :");
						pro[i]=sc.next();
					}
				}
			}
			System.out.print("Proposition sans Erreur : ");
			for (int i=0;i<nbatrouver;i++) {
				this.propositionu[i]=Integer.parseInt(pro[i]);
				System.out.print(pro[i]);	
			}
			System.out.print("\n");
			break;

		case "defenseur":
			String proordi="";

			if(this.tour+1<=nbcouleur) {
				for(int i=0;i<nbatrouver;i++) {
					this.propositiono[i]=this.tour;
				}
			}else {
				this.propositiono=this.toutepos.get((int)(Math.random() * ((this.toutepos.size())-0)) + 0);
			}

			//affichage de la proposition
			for(int i=0;i<nbatrouver;i++) {
				proordi=proordi+this.propositiono[i];
			}
			System.out.println("L'ordinateur a propos� : "+ proordi);
		}
	}

	/**
	 * M�thode servant � trait�s les compositions ne contenant pas <br>
	 * le chiffre bien plac�.
	 * @param nbatrouver
	 *  			Nombre de chiffre � trouver pour gagner
	 * @param placedunombrebienplace
	 * 				La place dans la combinaison du chiffre bien place
	 */
	private void traitementdubienplace(int nbatrouver, int placedunombrebienplace) {
		TreeSet<Integer> Combaenlever =new TreeSet<Integer>();
		for(int i=0;i<this.toutepos.size();i++) {
			if(this.propositiono[placedunombrebienplace]!=this.toutepos.get(i)[placedunombrebienplace]) {
				Combaenlever.add(i);
			}
		}
		while(Combaenlever.isEmpty()==false) {
			/*for(int i=0;i<4;i++) {
				System.out.print(this.toutepos.get(Combaenlever.last().intValue())[i]);
			}
			System.out.println();*/
			this.toutepos.remove(Combaenlever.pollLast().intValue());//suppresion des combinaisons non voulues
		}
	}

	/**
	 * M�thode servant � trait�s les compositions ne contenant pas <br>
	 * les chiffres pr�sents et les chiffres bien plac�s.
	 * @param nbatrouver
	 *  			Nombre de chiffre � trouver pour gagner
	 */
	private void tratitementdesmalplace(int nbatrouver) {
		TreeSet<Integer> Combaenlever =new TreeSet<Integer>();
		for(int i=0;i<toutepos.size();i++) {
			for(int j=0;j<nbatrouver;j++) {
				for(int k=0;k<nbatrouver;k++) {
					if(propositiono[j]==toutepos.get(i)[k]) {
						Combaenlever.add(i);
					}
				}
			}
		}

		while(Combaenlever.isEmpty()==false) {
			this.toutepos.remove(Combaenlever.pollLast().intValue());//suppresion des combinaisons non voulues
		}

	}


	/**
	 * M�thode servant � trait�s les compositions ne contenant pas <br>
	 * le ou les chiffres pr�sents.
	 * @param nbatrouver
	 *  			Nombre de chiffre � trouver pour gagner
	 */
	private void traitementdespresent(int nbatrouver) {
		TreeSet<Integer> Combaenlever =new TreeSet<Integer>();	



		for(int i=0;i<toutepos.size();i++) {
			boolean trouver=true;
			for(int j=0;j<nbatrouver && trouver==true;j++) {
				for(int k=0;k<nbatrouver && trouver==true;k++) {
					if(this.propositiono[j]==this.toutepos.get(i)[k]) {
						trouver=false;
						Combaenlever.remove(i);
					}else {
						trouver=true;
						Combaenlever.add(i);
					}
				}
			}


		}


		while(Combaenlever.isEmpty()==false) {
			this.toutepos.remove(Combaenlever.pollLast().intValue());
		}

	}


	/**
	 * M�thode servant � verifier les propositions de l'ordinateur et de l'utilisateur selon le mode choisi.
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param modedej
	 * 			Mode de Jeu a laquelle on joue
	 */
	private void verification(int nbatrouver,String modedej) {

		switch(modedej) {
		case "challenger":
			this.bienplaceu=0;
			this.presentu=0;
			int[] clonePropou   = Arrays.copyOf(propositionu, propositionu.length);
			int[] clonecombiu = Arrays.copyOf(this.combinaisonu, this.combinaisonu.length);

			//d�nombrement des bien plac�s
			for (int i=0; i<nbatrouver; i++) {
				if ( clonePropou[i]==clonecombiu[i] ) {
					bienplaceu++;
					clonePropou[i]   = -1;
					clonecombiu[i] = -2;
				}
			}

			//d�nombrement des pr�sents
			for (int i=0; i<nbatrouver; i++) {
				for (int j=0; j<nbatrouver; j++) {
					if ( clonePropou[i]==clonecombiu[j] ) {
						presentu++;
						clonePropou[i]   = -1;
						clonecombiu[j] = -2;
					}
				}
			}
			System.out.println("Reponse : "+presentu+" pr�sent, "+bienplaceu+" bien plac�");
			break;

		case "defenseur":
			this.bienplaceo=0;
			this.presento=0;
			int[] clonePropoo   = Arrays.copyOf(propositiono, propositiono.length);
			int[] clonecombio = Arrays.copyOf(this.combinaisono, this.combinaisono.length);

			//d�nombrement des bien plac�s
			for (int i=0; i<nbatrouver; i++) {
				if ( clonePropoo[i]==clonecombio[i] ) {
					bienplaceo++;
					clonePropoo[i]   = -1;
					clonecombio[i] = -2;
				}
			}

			//d�nombrement des pr�sents
			for (int i=0; i<nbatrouver; i++) {
				for (int j=0; j<nbatrouver; j++) {
					if ( clonePropoo[i]==clonecombio[j] ) {
						presento++;
						clonePropoo[i]   = -1;
						clonecombio[j] = -2;
					}
				}
			}
			System.out.println("Reponse : "+presento+" pr�sent, "+bienplaceo+" bien plac�");
			break;

		}
	}

	/**
	 * M�thode permettant de supprimer les combinaisons non voulus.
	 * @param nbatrouver
	 *  			Nombre de chiffre � trouver pour gagner
	 * @param nbcouleur
	 *  			Nombre de couleurs utilis�s dans le jeu
	 *  @param triche
	 * 		Mode triche : Vrai pour l'activer, Faux pour ne pas l'activer
	 *  
	 */
	private void suppresioncombi(int nbatrouver,int nbcouleur,boolean triche) {

		if(triche==true&&bienplaceo!=0) {
			for(int i=0;i<nbatrouver;i++) {
				if(combinaisono[i]==propositiono[i]) {

					traitementdubienplace(nbatrouver, i);
				}
			}
		}


		//Suppresion de la proposition fausse
		this.toutepos.remove(propositiono);

		//Traitement des presents
		if(presento!=0||bienplaceo!=0) {
			traitementdespresent(nbatrouver);
		}

		//Traitement des non presents
		if((presento==0)&&(bienplaceo==0)) {
			tratitementdesmalplace(nbatrouver);
		}

	}




	/**
	 * Fonction servant � mettre sous forme d'une chaine de caract�re la solution.
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @return soluce
	 * 			Chaine de caract�re de la soluce
	 */
	private String affichersoluce(int nbatrouver) {
		String soluce="";
		for(int i=0;i<nbatrouver;i++) {

			soluce=soluce+this.combinaisonu[i];
		}
		return soluce;
	}

	/**
	 * M�thode permettant de g�nerer toute les possibilit�s de <br>
	 * couleur puissance nbatrouver.
	 * 
	 * @param nbatrouver
	 * 			Nombre de chiffre � trouver pour gagner
	 * @param couleur
	 * 			Nombre de couleurs utilis�s dans le jeu
	 */
	private void genererpos(int nbatrouver,int couleur) {
		int max=(int) Math.pow(couleur, nbatrouver);
		int[] pos=new int[nbatrouver];
		for(int i=0;i<max;i++) {
			pos=convertionversbase(i, couleur, nbatrouver);
			this.toutepos.add(pos);
		}

	}


	/**
	 * Fonction permmettant de convertir un nombre , en base dix, <br>
	 * en une nouvelle base. 
	 * @param nbr
	 * 				Nombre � convertir
	 * @param nouvel_base
	 * 				La nouvelle base utilis� pour convertir le nombre
	 * @param nbatrouver
	 *  			Nombre de chiffre � trouver pour gagner
	 * @return une ligne de tableau comportant le nombre convertie 
	 */
	private static int[] convertionversbase(int nbr,int nouvel_base,int nbatrouver) {
		char[] symbol = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
		String res="";
		int[] resultat=new int[nbatrouver];

		while(nbr!=0) {
			res=symbol[nbr%nouvel_base]+res;
			nbr=(int) Math.floor(nbr/nouvel_base);
		}
		StringBuffer st =new StringBuffer();
		for(int i=0;i<nbatrouver-res.length();i++) {
			st.append(0);
		}
		st.append(res);

		for(int i=0;i<nbatrouver;i++) {
			resultat[i]=Integer.parseInt(new String(st).split("")[i]);
		}

		return resultat;

	}
}



