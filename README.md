<h1> OC Parcours DA JAVA Projet 3 </h1>

<h2> Installation </h2>

<h3> Maven </h3> 

Ce projet est un projet maven donc l'installation de maven est requis <br>
Vous pouvez télécharger maven ici : 

> https://maven.apache.org/download.cgi

Et suivre les instructions d'installation ici :
> https://maven.apache.org/install.html

<h3> Installation du projet </h3>

* Télécharger le projet ou dans une fenetre git bash utiliser cette commande :

`git clone https://gitlab.com/Aruldir/da_java_p3.git`

* Mettez vous à la racine du projet que vous avez télécharger et taper dans un cmd :

`mvn install`

<h2> Utilisation </h2>

Pour modifier les paramètres de l'application, il faut aller dans le dossier <br>
src puis main puis ressources et modifier les valeurs dans le fichier config.properties.<br>
> Pour prendre en compte ses modifications il est nécessaire de regénérer le .jar en faisant : <br>
>* mvn clean 
>* mvn install

* Pour lancer l'application, allez dans le dossier target et taper dans un cmd :

`java -jar Mastermind-Recherche_App.jar`

* Pour lancer l'application en mode developpeur, allez dans le dossier target et taper dans un cmd :

`java -jar Mastermind-Recherche_App.jar modedev`

<h2> Désinstallation </h2>

* Pour désinstaller l'application, placez vous à la racine du projet et tapez dans un cmd :

`mvn clean`



